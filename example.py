import waveform
import math
import os

import numpy as np


#Generator provides python object interface
#lets make an instace called wf
wf=waveform.waveform()

#Add some pulses

wf.pulse(start=2, duration=1, amplitude = 0.3)
#start and duration are in miliseconds
#amplitude in in range 0 to 1 for gas puffing, -1 to 1 for stabilization

#argument names are optinal
wf.pulse(4, 2, -0.2)



#Now add some ramps
#same as pulses, but they have 
wf.ramp(start = 8, duration = 5, start_amplitude=0.1, end_amplitude=0.4)

#everything may be superposed
wf.ramp(15, 4, 0.4, 0.2)
wf.pulse(16, 2, - 0.2)

#now with functions
#first define function with one numerical argument
def f(x):
	return(0.5*math.sin(x))

#prepare some costants
pi=math.pi

#Inserting functions
wf.function (start=20, duration=2*pi, func=f)
#note, that function will be given time in miliseconds
#time is not time from begining of waveform, but starts at zero


#Finally, interpoaltion
file = np.loadtxt("input.txt")
t=file[:, 0]
x=file[:, 1]

wf.interpolate(start=30, duration=10, t=t, x=x, kind="slinear")
#time is not time from begining of waveform, but starts at zero

#now plot it
wf.plot(end=40)

#and output it
wf.generate("./waveform.txt")